# Chris Titus Tech's Windows Utility

This Utility is a compilation of windows tasks I perform on each Windows system I use. It is meant to streamline *installs*, debloat with *tweaks*, troubleshoot with *config*, and fix Windows *updates*. I am extremely picky on any contributions to keep this project clean and efficient. 

![screen-install](screen-install.png)

**Launch Command: (It will launch my edited script)**

```
iwr -useb https://bit.ly/windows-util | iex
```

EXE Wrapper for $5 @ https://www.cttstore.com/windows-toolbox

## Overview

- Install
  - Installs all selected programs
- Tweaks
  - Optimizes windows and reduces running processes
- Config
  - Quick configurations for Windows Installs
- Updates
  - Fixes the default windows update scheme


## Changelogs

- Added Programs to Installer
  - TeamSpeak
  - WhatsApp
  - Telegram
  - Ubisoft Connect
  - Origin
  - Valorant
  - Autoruns
  
- Added Redistributables to Configs
  - Visual C++
  - DirectX
  
- Added Driver Installer to Configs
  - Snappy Driver Installer Origin
  
  
